import java.io.PrintStream;

public class BinaryTreePrinter {
    private Colors color = new Colors();

    private AVLTree tree;

    public BinaryTreePrinter(AVLTree tree) {
        this.tree = tree;
    }

    private String traversePreOrder() {

        if (tree.getRoot() == null) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        sb.append(tree.getRoot().getValue());

        String pointerRight = "└──";
        String pointerLeft = (tree.getRoot().getRight() != null) ? "├──" : "└──";

        traverseNodes(sb, "", pointerLeft, tree.getRoot().getLeft(), tree.getRoot().getRight() != null);
        traverseNodes(sb, "", pointerRight, tree.getRoot().getRight(), false);

        return sb.toString();
    }

    private void traverseNodes(StringBuilder sb, String padding, String pointer, AVLTree.Node node,
        boolean hasRightSibling) {

        if (node != null) {

            sb.append("\n");
            sb.append(padding);
            sb.append(pointer);
            sb.append(node.getValue());

            StringBuilder paddingBuilder = new StringBuilder(padding);
            if (hasRightSibling) {
                paddingBuilder.append("│  ");
            } else {
                paddingBuilder.append("   ");
            }

            String paddingForBoth = paddingBuilder.toString();
            String pointerRight = "└──";
            String pointerLeft = (node.getRight() != null) ? "├──" : "└──";

            traverseNodes(sb, paddingForBoth, pointerLeft, node.getLeft(), node.getRight() != null);
            traverseNodes(sb, paddingForBoth, pointerRight, node.getRight(), false);

        }

    }

    public void print(PrintStream os) {
        os.print(traversePreOrder());
    }

}
